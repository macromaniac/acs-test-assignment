package utils

import models.{Category, SaleUnit}
/**
  * Created by andrey on 11/23/16.
  */
trait ModelStubHelpers {

  def book(description: String, count: Int, unitPrice: Double): SaleUnit = {
    SaleUnit(description, Set(Category.BOOKS), imported = false, count, unitPrice)
  }

  def importedBook(description: String, count: Int, unitPrice: Double): SaleUnit = {
    SaleUnit(description, Set(Category.BOOKS), imported = true, count, unitPrice)
  }

  def food(description: String, count: Int, unitPrice: Double): SaleUnit = {
    SaleUnit(description, Set(Category.FOOD), imported = false, count, unitPrice)
  }

  def importedFood(description: String, count: Int, unitPrice: Double): SaleUnit = {
    SaleUnit(description, Set(Category.FOOD), imported = true, count, unitPrice)
  }

  def importedMeds(description: String, count: Int, unitPrice: Double): SaleUnit = {
    SaleUnit(description, Set(Category.MEDS), imported = true, count, unitPrice)
  }

  def meds(description: String, count: Int, unitPrice: Double): SaleUnit = {
    SaleUnit(description, Set(Category.MEDS), imported = false, count, unitPrice)
  }

  def uncategorizedSaleUnit(description: String, count: Int, unitPrice: Double): SaleUnit = {
    SaleUnit(description, Set(), imported = false, count, unitPrice)
  }
  def importedUncategorizedSaleUnit(description: String, count: Int, unitPrice: Double): SaleUnit = {
    SaleUnit(description, Set(), imported = true, count, unitPrice)
  }


}
