package unit

import org.scalatestplus.play.PlaySpec
import services.taxRules.{BaseTaxRule, FreeCategoriesTaxRule, ImportedBaseRule}
import services.{RuleBasedTaxCalculator}
import utils.ModelStubHelpers

/**
  * Created by andrey on 11/21/16.
  */
class TaxCalculatorTests extends PlaySpec with ModelStubHelpers {
  val ruleBasedTaxCalculator = new RuleBasedTaxCalculator

  "RuleBasedTaxCalculator" should {
    "correctly calculate summary tax for sale items (without imported)" in {

      val saleUnits = Seq(
        book("Book", 1, 12.49D),
        uncategorizedSaleUnit("Chocolate Bar", 1, 0.85D),
        uncategorizedSaleUnit("Music CD", 1, 14.99D)
      )
      ruleBasedTaxCalculator.calculate(saleUnits) mustBe (1.58D +- 0.01)

    }
    "correctly calculate summary tax for sale items (with imported)" in {
      val saleUnits = Seq(
        book("Book", 1, 12.49D),
        uncategorizedSaleUnit("Chocolate Bar", 1, 0.85D),
        importedUncategorizedSaleUnit("Music CD", 1, 14.99D)
      )
      ruleBasedTaxCalculator.calculate(saleUnits) mustBe (2.33D +- 0.01)
    }

    "correctly calculate summary tax for free of tax goodies" in {
      val saleUnits = Seq(
        book("Book", 1, 12.49D),
        meds("Chocolate Bar", 1, 0.85D),
        food("Chips", 1, 14.99D)
      )
      ruleBasedTaxCalculator.calculate(saleUnits) mustBe 0D
    }
  }
  "TaxRule functions" when {
    "applied" should {
      "correctly calculate result as baseTaxRule for book(\"Book\",1,20D)" in {
        new BaseTaxRule(book("Book", 1, 20D)).calculate(0) mustBe 2D
      }
      "correctly calculate result as baseTaxRule for uncategorizedSaleUnit(\"Socks\",2,12.5D)" in {
        new BaseTaxRule(uncategorizedSaleUnit("Socks", 2, 12.5D)).calculate(0) mustBe 2.5D
      }
      "correctly calculate result for FOOD as freeCategoriesTaxRule" in {
        new FreeCategoriesTaxRule(food("Chips", 1, 20D)).calculate(5) mustBe 0D
      }
      "correctly calculate result for BOOKS as freeCategoriesTaxRule" in {
        new FreeCategoriesTaxRule(book("Book", 1, 20D)).calculate(5) mustBe 0D
      }
      "correctly calculate result for MEDS as freeCategoriesTaxRule" in {
        new FreeCategoriesTaxRule(meds("Pills", 1, 20D)).calculate(5) mustBe 0D
      }
      "correctly calculate result for non-free categories as freeCategoriesTaxRule" in {
        new FreeCategoriesTaxRule(uncategorizedSaleUnit("Pills", 1, 20D)).calculate(5) mustBe 5D
      }
      "correctly calculate result for imported categories as importedBaseRule" in {
        new ImportedBaseRule(importedFood("Chips", 1, 20D)).calculate(5) mustBe 6D
      }
      "correctly calculate result for non-imported categories as importedBaseRule" in {
        new ImportedBaseRule(food("Chips", 1, 20D)).calculate(5) mustBe 5D
      }
    }

  }
}
