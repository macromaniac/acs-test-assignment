package unit

import akka.stream.Materializer
import controllers.TaxCalculationController
import models.{Category, SaleUnit}
import org.scalatestplus.play.{OneAppPerSuite, PlaySpec}
import play.api.http.{ContentTypes, HeaderNames}
import play.api.mvc.AnyContentAsEmpty
import play.api.test.Helpers._
import play.api.test.{FakeHeaders, FakeRequest}

/**
  * Created by andrey on 11/21/16.
  */
class ControllerTests extends PlaySpec
  with OneAppPerSuite {

  implicit lazy val materializer: Materializer = app.materializer

  "TaxCalculatorController#taxcalculator" should {
    val controller: TaxCalculationController = app.injector.instanceOf[TaxCalculationController]

    "fail if Content-Type is not application/json" in {
      val resultAccumulator = controller.taxCalculator.apply(FakeRequest("POST", "/",
        FakeHeaders(), AnyContentAsEmpty))

      status(resultAccumulator.run()) mustBe UNSUPPORTED_MEDIA_TYPE
    }

    "accept only Content-Type:application/json" in {
      val request = FakeRequest("POST", "/")
        .withHeaders(HeaderNames.CONTENT_TYPE -> ContentTypes.JSON)
        .withBody(Seq(SaleUnit("Bar", Set(Category.FOOD), imported = true, 1, 1)))

      status(controller.taxCalculator.apply(request)) mustBe OK
    }
  }
}
