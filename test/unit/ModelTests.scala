package unit

import models.{SaleUnit, TaxResponse}
import org.scalatestplus.play.PlaySpec
import org.skyscreamer.jsonassert.JSONAssert
import play.api.libs.json._
import utils.ModelStubHelpers

/**
  * Created by andrey on 11/21/16.
  */
class ModelTests extends PlaySpec
  with ModelStubHelpers {

  "SaleUnit model" should {
    "be marshalled from json" in {
      val saleUnitsJson: JsValue = Json.parse(
        """[
          |  {
          |    "description": "Book",
          |    "categories": [
          |      "BOOKS"
          |    ],
          |    "imported": false,
          |    "count": 1,
          |    "unitPrice": 12.49
          |  }
          |]
          |
        """.stripMargin('|'))
      val marshalledResult = Json.fromJson[Seq[SaleUnit]](saleUnitsJson)

      val saleUnits: Seq[SaleUnit] = marshalledResult.get

      val firstSaleUnit = saleUnits.head
      firstSaleUnit mustBe book("Book", 1, 12.49D)
    }

    "fail marshalling if count is negative" in {
      val saleUnitsJson: JsValue = Json.parse(
        """[
          |  {
          |    "description": "Book",
          |    "categories": [
          |      "BOOKS"
          |    ],
          |    "imported": false,
          |    "count": -1,
          |    "unitPrice": 12.45
          |  }
          |]
          |
        """.stripMargin('|'))

      val result = Json.fromJson[Seq[SaleUnit]](saleUnitsJson)
      result.isError mustBe true
    }

    "fail marshalling if unitPrice is negative" in {
      val saleUnitsJson: JsValue = Json.parse(
        """[
          |  {
          |    "description": "Book",
          |    "categories": [
          |      "BOOKS"
          |    ],
          |    "imported": false,
          |    "count": 1,
          |    "unitPrice": -12.45
          |  }
          |]
          |
        """.stripMargin('|'))

      val result = Json.fromJson[Seq[SaleUnit]](saleUnitsJson)
      result.isError mustBe true
    }

    "be unmarshalled to json" in {

      val saleUnits = Seq(
        uncategorizedSaleUnit("CD", 2, 14.89D)
      )
      val actualJson = Json.toJson[Seq[SaleUnit]](saleUnits).toString()
      JSONAssert.assertEquals(
        """[
          |  {
          |    "description": "CD",
          |    "categories": [],
          |    "imported": false,
          |    "count": 2,
          |    "unitPrice": 14.89
          |  }
          |]""".stripMargin('|'), actualJson, false)

    }
  }
  "TaxResponse model" should {
    "be marshalled from json" in {
      val taxResponseJson: JsValue = Json.parse("""{"salesTax":1.50}""")
      getAsNumberValue(taxResponseJson, "salesTax").toDouble mustBe 1.50D
    }
    "be unmarshalled to json" in {
      val taxResponse = TaxResponse(salesTax = 2.45D)
      val actualJson = Json.toJson(taxResponse).toString()
      JSONAssert.assertEquals("""{"salesTax":2.45}""", actualJson, false)
    }
    "do correct rounding with scale=2 for 2.45899" in {
      val taxResponse = TaxResponse(salesTax = 2.45899D)
      val jsonTaxResponse = Json.toJson(taxResponse)
      getAsNumberValue(jsonTaxResponse, "salesTax").toDouble mustBe 2.46D
    }

    "do correct rounding with scale=2 for 2.532399" in {
      val taxResponse = TaxResponse(salesTax = 2.532399D)
      val jsonTaxResponse = Json.toJson(taxResponse)
      getAsNumberValue(jsonTaxResponse, "salesTax").toDouble mustBe 2.53D
    }

  }
  def getAsNumberValue(jsValue: JsValue, fieldName: String): BigDecimal = {
    (jsValue \ fieldName).get.as[JsNumber].value
  }
  def getAsNumberValue(jsLookupResult: JsLookupResult, fieldName: String): BigDecimal = {
    (jsLookupResult \ fieldName).get.as[JsNumber].value
  }
  def getAsArrayValue(jsLookupResult: JsLookupResult, fieldName: String): Seq[JsValue] = {
    (jsLookupResult \ fieldName).get.as[JsArray].value
  }

  def getAsStringValue(jsLookupResult: JsLookupResult, fieldName: String): String = {
    (jsLookupResult \ fieldName).get.as[JsString].value
  }
  def getAsBooleanValue(jsLookupResult: JsLookupResult, fieldName: String): Boolean = {
    (jsLookupResult \ fieldName).get.as[JsBoolean].value
  }

}
