package functional

import controllers.TaxCalculationController
import models.TaxResponse
import org.scalatestplus.play._
import play.api.cache.EhCacheModule
import play.api.http.{ContentTypes, HeaderNames}
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.JsNumber
import play.api.libs.ws.{WSClient, WSResponse}
import play.api.mvc.{Action, _}
import play.api.routing.Router
import play.api.routing.sird._
import play.api.test.Helpers.testServerPort
import play.api.test._
import services.RuleBasedTaxCalculator

/**
  * Created by andrey on 11/23/16.
  */

class AcceptanceCases extends PlaySpec
  with FutureAwaits
  with DefaultAwaitTimeout
  with OneServerPerSuite
{
  implicit override lazy val app =
    new GuiceApplicationBuilder().disable[EhCacheModule].router(Router.from {
      case GET(p"/") => Action {
        Results.Ok("ok")
      }
      case POST(p"/taxcalculator") => new TaxCalculationController(new RuleBasedTaxCalculator).taxCalculator
    }).build()
  val wsClient = app.injector.instanceOf[WSClient]

  "Tax calculator" must {

    "correctly do calculation for input\n " +
      "1 book at 12.49\n" +
      "1 musicCD at 14.99\n" +
      "1 chocolate bar at 0.85\n" +
      " and return salesTax = 1.50}" in {

      val itemsJson =
        """[
          |  {
          |    "description": "book",
          |    "categories": [
          |      "BOOKS"
          |    ],
          |    "imported": false,
          |    "count": 1,
          |    "unitPrice": 12.49
          |  },
          |  {
          |    "description": "chocolate bar",
          |    "categories": [
          |      "FOOD"
          |    ],
          |    "imported": false,
          |    "count": 1,
          |    "unitPrice": 0.85
          |  },
          |  {
          |    "description": "music CD",
          |    "imported": false,
          |    "categories": [],
          |    "count": 1,
          |    "unitPrice": 14.99
          |  }
          |]""".stripMargin('|')

      sendItemsAndCheckSalesTax(itemsJson, 1.50D)

    }
    "correctly do calculation for input\n " +
      "1 imported box of chocolates at 10.00\n" +
      "1 imported bottle of perfume at 47.50\n" +
      " and return salesTax = 7.65}" in {

      val itemsJson =
        """[
          |  {
          |    "description": "bottle of perfume",
          |    "categories": [],
          |    "imported": true,
          |    "count": 1,
          |    "unitPrice": 47.50
          |  },
          |  {
          |    "description": "chocolates",
          |    "categories": [
          |      "FOOD"
          |    ],
          |    "imported": true,
          |    "count": 1,
          |    "unitPrice": 10.00
          |  }
          |]""".stripMargin('|')

      sendItemsAndCheckSalesTax(itemsJson, 7.65D)

    }
    "correctly do calculation for input " +
      "1 imported bottle of perfume at 27.99\n" +
      "1 bottle of perfume at 18.99\n" +
      "1 packet of headache pills at 9.75\n" +
      "1 box of imported chocolates at 11.25\n" +
      " and return salesTax = 6.70}" in {

      val itemsJson =
        """[
          |  {
          |    "description": "bottle of perfume",
          |    "categories": [],
          |    "imported": true,
          |    "count": 1,
          |    "unitPrice": 27.99
          |  },
          |  {
          |    "description": "bottle of perfume",
          |    "categories": [],
          |    "imported": false,
          |    "count": 1,
          |    "unitPrice": 18.99
          |  },
          |  {
          |    "description": "packet of headache pills",
          |    "categories": [
          |      "MEDS"
          |    ],
          |    "imported": false,
          |    "count": 1,
          |    "unitPrice": 9.75
          |  },{
          |    "description": "box of imported chocolates",
          |    "categories": [
          |      "FOOD"
          |    ],
          |    "imported": true,
          |    "count": 1,
          |    "unitPrice": 11.25
          |  }
          |]""".stripMargin('|')

      sendItemsAndCheckSalesTax(itemsJson, 6.70D)

    }
  }
  private def sendItemsAndCheckSalesTax(inputJson: String, salesTax: Double) = {
    val response = sendPostWithTextBody(inputJson)
    response.json.validate[TaxResponse].isSuccess mustBe true
    (response.json \ "salesTax").get.as[JsNumber].value.toDouble mustBe (salesTax +- 0.1D)
  }
  private def sendPostWithTextBody(body: String): WSResponse = {
    await(wsClient.url(s"http://localhost:$testServerPort/taxcalculator")
      .withHeaders(HeaderNames.CONTENT_TYPE -> ContentTypes.JSON)
      .post(body))
  }

}
