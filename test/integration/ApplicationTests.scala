package integration

import controllers.TaxCalculationController
import models.TaxResponse
import org.scalatestplus.play._
import play.api.cache.EhCacheModule
import play.api.http.{ContentTypes, HeaderNames}
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.ws.{WSClient, WSResponse}
import play.api.mvc.{Action, _}
import play.api.routing.Router
import play.api.routing.sird._
import play.api.test.Helpers.{OK, testServerPort}
import play.api.test._
import services.RuleBasedTaxCalculator

import scala.concurrent.Future

/**
  * Created by andrey on 11/21/16.
  */
class ApplicationTests extends PlaySpec with OneServerPerSuite
  with FutureAwaits
  with DefaultAwaitTimeout {

  implicit override lazy val app =
    new GuiceApplicationBuilder().disable[EhCacheModule].router(Router.from {
      case GET(p"/") => Action {
        Results.Ok("ok")
      }
      case POST(p"/taxcalculator") => new TaxCalculationController(new RuleBasedTaxCalculator).taxCalculator
    }).build()

  val wsClient = app.injector.instanceOf[WSClient]

  "Tax Calculator http-service" should {
    "be reacheable" in {
      val response = await(wsClient.url(s"http://localhost:$testServerPort/").get())
      response.status mustBe OK
    }

    "be mapped to '/taxcalculator' endpoint" in {
      val response = await(sendPostWithFile)
      response.status mustBe OK
    }

    "answer with correct json" in {
      val response = await(sendPostWithFile)
      response.json.validate[TaxResponse].isSuccess mustBe true
    }
  }
  def sendPostWithFile: Future[WSResponse] = {
    wsClient.url(s"http://localhost:$testServerPort/taxcalculator")
      .withHeaders(HeaderNames.CONTENT_TYPE -> ContentTypes.JSON)
      .post(app.getFile("/test/resources/input1.json"))
  }

}

