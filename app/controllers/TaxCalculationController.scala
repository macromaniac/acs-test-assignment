package controllers

import com.google.inject.Inject
import models.{SaleUnit, TaxResponse}
import play.api.libs.json._
import play.api.mvc.{Action, BodyParsers, Controller}
import services.RuleBasedTaxCalculator

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by andrey on 11/21/16.
  */
class TaxCalculationController @Inject()(taxCalculatorService: RuleBasedTaxCalculator) extends Controller {

  def taxCalculator = Action(validateJson[Seq[SaleUnit]]) {
    request =>
      val saleUnits = request.body
      val salesTax: Double = taxCalculatorService.calculate(saleUnits)
      val response = TaxResponse(salesTax)
      Ok(Json.toJson(response))
  }

  /**
    * Body parser
    * validates:
    *        1. ContentType:application/json or text/json
    *        2. try to build model from json body by context bounded Reads[T]
    */

  def validateJson[A: Reads] = BodyParsers.parse.json.validate(
    _.validate[A].asEither.left.map(e => BadRequest(JsError.toJson(e)))
  )


}
