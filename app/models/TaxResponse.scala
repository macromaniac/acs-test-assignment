package models

import play.api.libs.json.{JsPath, Json, Reads, Writes}
import play.api.libs.functional.syntax._

/**
  * Created by andrey on 11/24/16.
  */
/**
  * Sales tax result value object
  *
  * @param salesTax
  */
case class TaxResponse(salesTax: Double)

object TaxResponse {

  implicit val taxResponseWrites: Writes[TaxResponse] = (JsPath \ "salesTax")
    .write[Double].contramap { c: TaxResponse => round(c.salesTax) }

  implicit val taxResponseReads: Reads[TaxResponse] = Json.reads[TaxResponse]

  def round(value: Double) = BigDecimal(value).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
}