package models

import models.Category.Category
import play.api.libs.functional.syntax.unlift
import play.api.libs.json.{JsPath, Reads, Writes}
import play.api.libs.functional.syntax._

/**
  * Created by andrey on 11/24/16.
  */

/**
  * Product category enumeration
  */
object Category extends Enumeration {
  type Category = Value
  val BOOKS, MEDS, FOOD = Value
}

/**
  * Sale item entity
  *
  * @param description
  * @param categories BOOKS/MEDS/FOOD
  * @param imported   true/false
  * @param count
  * @param unitPrice
  */

case class SaleUnit(description: String, categories: Set[Category], imported: Boolean, count: Int, unitPrice: Double)

object SaleUnit {
  implicit val categoryReads = Reads.enumNameReads(Category)
  implicit val salesUnitReads: Reads[SaleUnit] = (
    (JsPath \ "description").read[String]
      and (JsPath \ "categories").read[Set[Category]]
      and (JsPath \ "imported").read[Boolean]
      and (JsPath \ "count").read[Int](Reads.min(1))
      and (JsPath \ "unitPrice").read[Double](Reads.min(0D))
    ) (SaleUnit.apply _)

  implicit val saleUnitsWrites: Writes[SaleUnit] = (
    (JsPath \ "description").write[String]
      and (JsPath \ "categories").write[Set[Category]]
      and (JsPath \ "imported").write[Boolean]
      and (JsPath \ "count").write[Int]
      and (JsPath \ "unitPrice").write[Double]
    ) (unlift(SaleUnit.unapply))

}