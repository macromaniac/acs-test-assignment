package services.taxRules

/**
  * Created by andrey on 11/24/16.
  */
import models.{Category, SaleUnit}
import services.TaxCalculator._

sealed abstract class TaxRule(saleUnit: SaleUnit) {
  //some types of rules may demand result from previous calculation step
  def calculate(previous: Double): Double
}

class BaseTaxRule(saleUnit: SaleUnit) extends TaxRule(saleUnit: SaleUnit) {
  val BASE_TAX_COEF = 0.1D
  override def calculate(previous: Double): Double = {
    previous + (saleUnit.price() * BASE_TAX_COEF)
  }
}

class FreeCategoriesTaxRule(saleUnit: SaleUnit) extends TaxRule(saleUnit: SaleUnit) {
  val ZERO_TAX = 0D
  override def calculate(previous: Double): Double = {
    if (saleUnit.categories.intersect(
      Set(Category.BOOKS,
        Category.FOOD,
        Category.MEDS)).nonEmpty) ZERO_TAX
    else previous
  }
}

class ImportedBaseRule(saleUnit: SaleUnit) extends TaxRule(saleUnit: SaleUnit) {
  val IMPORT_TAX_COEF = 0.05D
  override def calculate(previous: Double): Double = {
    if (saleUnit.imported) previous + (saleUnit.price() * IMPORT_TAX_COEF)
    else previous
  }
}