package services

import models.SaleUnit


/**
  * Created by andrey on 11/21/16.
  */

//base interface for tax calculator
trait TaxCalculator {

  //whole sequence is passed to method to have complete access to sale
  def calculate(saleUnits: Seq[SaleUnit]): Double

}
object TaxCalculator {
  implicit class SaleUnitOps(saleUnit: SaleUnit) {
    def price(): Double = saleUnit.unitPrice * saleUnit.count
  }
}


