package services

import models.SaleUnit
import services.taxRules.{BaseTaxRule, FreeCategoriesTaxRule, ImportedBaseRule}

/**
  * Created by andrey on 11/21/16.
  *
  * Tax calculator based on defined calculation rules order
  */
class RuleBasedTaxCalculator extends TaxCalculator {

  override def calculate(saleUnits: Seq[SaleUnit]): Double = {
    saleUnits.foldLeft(0D)((previousSalesTax, saleUnit) =>
      previousSalesTax + ruleCombination(saleUnit)(0D))
  }

  def ruleCombination(saleUnit: SaleUnit): Double => Double = {
    (new BaseTaxRule(saleUnit).calculate _)
      .andThen(new FreeCategoriesTaxRule(saleUnit).calculate _)
      .andThen(new ImportedBaseRule(saleUnit).calculate _)
  }
}


